# Ciao-Ciao 
<img src="https://codeberg.org/theclosedbitter/Ciao-Ciao/raw/branch/main/screenshotofciao-ciao.png" alt="picture of ciao-ciao">
A logout program/manager written in <a href="https://www.youtube.com/watch?v=tas0O586t80">C</a> inspired by derek taylor is bye bye but instead of being written in Haskel its written in <a href="https://www.youtube.com/watch?v=tas0O586t80">C</a> and has a binary provided by deafult

# Dependencies 
so lets get the obvious out of the way in order to run ciao-ciao you must have GTK and its Dependencies installed just to run 

# Compiling 
in order to compile Ciao-Ciao you need to have a gcc or a compiler have make installed have GTK and its Dependencies installed.
To compile Ciao Ciao do the following 
git clone https://codeberg.org/theclosedbitter/Ciao-Ciao.git
cd Ciao-Ciao
make install 
./Ciao-Ciao to run 

# Installing 
if you want to install you must have a binary and the glade file and the icons must in order to run 
To install without Compiling go to the releases tab or https://codeberg.org/attachments/a352fa76-1c19-4add-8c9a-08c95ddf4903
here unzip and run no Compiling required or go wget https://codeberg.org/attachments/a352fa76-1c19-4add-8c9a-08c95ddf4903 to directly install to command line

# Compatiblity 
this is a very small gtk app which is written with GNU/Linux in mind so any way to get it to run on windows would require messing with the source code 


