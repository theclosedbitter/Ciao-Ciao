#include <stdlib.h>
#include <gtk/gtk.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gtk/gtkx.h>
#include <math.h>
#include <ctype.h>

// make them global
GtkWidget  *window;
GtkWidget  *fixed1;
GtkWidget  *button1;
GtkWidget  *button2;
GtkWidget  *button3;
GtkWidget  *button4;
GtkWidget  *button5;
GtkWidget  *button6;
GtkBuilder *builder;
int main(int argc, char *argv[]) {
	gtk_init(&argc, &argv); // intalize gtk
/*__________________________________________________________________
  establish contact with xml code used to adjust the widget settings
  __________________________________________________________________
*/
	builder = gtk_builder_new_from_file("ciao-ciao.glade");

	window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
        g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit),NULL);

	gtk_builder_connect_signals(builder,NULL);

	fixed1 = GTK_WIDGET(gtk_builder_get_object(builder, "fixed1"));
        button1 = GTK_WIDGET(gtk_builder_get_object(builder, "button1"));
        button2 = GTK_WIDGET(gtk_builder_get_object(builder, "button2"));
        button3 = GTK_WIDGET(gtk_builder_get_object(builder, "button3"));
        button4 = GTK_WIDGET(gtk_builder_get_object(builder, "button4"));
        button5 = GTK_WIDGET(gtk_builder_get_object(builder, "button5"));
        button6 = GTK_WIDGET(gtk_builder_get_object(builder, "button6"));

	gtk_widget_show(window);
	gtk_main();
	return EXIT_SUCCESS;
}
void on_button1_clicked (GtkButton *b){ 
	system("shutdown now");
}
void on_button2_clicked (GtkButton *b) {
	system("reboot");
}
void on_button3_clicked (GtkButton *b) {
	system("gnome-session-quit");
	/* uncomment if you are building for another desktop enviornment 
	system("cinnamon-session-quit --logout --force");
	system("xfce4-session-logout");
	system("qdbus org.kde.ksmserver /KSMServer logout 0 0 0");*/
}
void on_button4_clicked (GtkButton * b) {
	system("systemctl suspend");
}
void on_button5_clicked (GtkButton * b) {
        system("killall ciao-ciao");
}
void on_button6_clicked (GtkButton * b) {
	system("systemctl hibernate");
}
